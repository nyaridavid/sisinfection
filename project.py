#!/bin/python


import sissim as sis
import random
import networkx
import copy
import collections
import matplotlib.pylab as plt
import matplotlib.pyplot as plot

class default_algorithm(sis.infection_algorithm):
    def __init__(self, infection_prob, cure_prob):
        self.p_infect = infection_prob
        self.p_cure = cure_prob
        self.known_neighbors = dict()

    def infect_new_nodes(self, graph, infected_nodes):
        neighbors = set();
        for node in infected_nodes:
            if node not in self.known_neighbors:
                self.known_neighbors[node] = graph.neighbors(node)
            neighbors.update(self.known_neighbors[node])

        newly_infected = set()
        for node in neighbors:
            if random.random() < self.p_infect:
                newly_infected.add(node)

        return newly_infected

    def cure_nodes(self, graph, infected_nodes):
        cured_nodes = set()
        for node in infected_nodes:
            if random.random() < self.p_cure:
                cured_nodes.add(node)

        return cured_nodes

class ws_model(sis.network_model):
    def __init__(self, number_of_nodes, neigbors_linked, rewire_probability):
        self.n = number_of_nodes
        self.ng = neigbors_linked
        self.beta = rewire_probability
        self.inited = False

    def get_graph(self):
        if self.inited:
            return self.g
        else:
            self.g = networkx.watts_strogatz_graph(self.n, self.ng, self.beta)
            self.inited = True
            return self.g



class ba_model(sis.network_model):
    def __init__(self, number_of_nodes, new_node_links):
        self.n = number_of_nodes
        self.m = new_node_links
        self.inited = False

    def get_graph(self):
        if self.inited:
            return self.g
        else:
            self.g = networkx.barabasi_albert_graph(self.n, self.m)
            self.inited = True
            return self.g

class ba_ws_inout(sis.network_model):
    def __init__(self, in_model, out_model):
        self.in_m = in_model
        self.out_m = out_model
        self.inited = False

    def get_graph(self):
        if self.inited:
            return self.g
        else:
            self.g = networkx.DiGraph()
            in_g = self.in_m.get_graph()
            out_g = self.out_m.get_graph()

            self.n = len(in_g.nodes())

            orig_nodes = in_g.nodes()

            exclude_out = set()
            exclude_in = set()
            for node in orig_nodes:
                out_neighs = out_g.neighbors(node)
                in_neighs = in_g.neighbors(node)
                for neig in out_neighs:
                    if (node, neig) not in exclude_out:
                        self.g.add_edge(node, neig)
                        exclude_out.add((neig, node))
                for neig in in_neighs:
                    if (neig, node) not in exclude_in:
                        self.g.add_edge(neig, node)
                        exclude_in.add((node, neig))
            self.inited = True
            return self.g

def average_n_runs(simulation, count, initnode = 1):
    output = collections.defaultdict(float)
    output_count = collections.defaultdict(float)
    modeltmp = simulation.model
    simulation.model = 0
    for i in range(1, count):
        simcopy = copy.deepcopy(simulation)
        simcopy.model = modeltmp
        simcopy.run_simulation(initnode)
        result = simcopy.get_proportionalized_output()
        result_len = len(result) - 1

        for time in range(0, result_len):
            output[time] += result[time]
            output_count[time] += 1.0

    output_list = list()
    outputlen = len(output) - 1
    for i in range(0, outputlen):
        count_get = output_count[i]
        if count_get >= count / 2:
            output_list.append(output[i] / output_count[i])

    simulation.model = modeltmp
    return output_list

sis.sis_simulator.print_progress = True

file_prefix='plots/'
def newplot():
    plot.clf()
    plot.xlabel('Timestep')
    plot.ylabel('Infected node proportion')
    plot.yscale('log')
    plot.xscale('log')


plot_it = False
to_file = True

def handle_output(filename, file_override = True):
        if to_file and file_override:
            plt.savefig(file_prefix+filename)
        if plot_it:
            plt.show()

def no_1_ws(to_file=True):
    model = ws_model(150000, 100, 0.1)
    algorithm = default_algorithm(0.05, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, p = {2}, inf_p = {3},\n cure_p = {4}, crit = {5}, reached = {6}".format( \
        model.n, avg_node_degree, model.beta, algorithm.p_infect, algorithm.p_cure, algorithm.p_cure / avg_node_degree, \
        simulation.get_equilibrium_proportion()))
    plot.plot(result)
    handle_output('no_1_ws.svg', to_file)
    return result

def no_2_ws(to_file=True):
    model = ws_model(150000, 100, 0.1)
    algorithm = default_algorithm(0.0005, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, p = {2}, inf_p = {3},\n cure_p = {4}, crit = {5}, reached = {6}".format( \
        model.n, avg_node_degree, model.beta, algorithm.p_infect, algorithm.p_cure, algorithm.p_cure / avg_node_degree, \
        simulation.get_equilibrium_proportion()))
    plot.plot(result)
    handle_output('no_2_ws.svg', to_file)
    return result

def no_3_ws(to_file=True):
    model = ws_model(150000, 100, 0.1)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 10000.0
        algorithm = default_algorithm(0.0012 + infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(10) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, p = {2}, inf_p = 0.0012-0.0022,\n cure_p = {3}, crit = 0.001".format( \
        model.n, avg_node_degree, model.beta, algorithm.p_cure))

    handle_output('no_3_ws.svg', to_file)
    return result

def no_4_ws(to_file=True):
    model = ws_model(150000, 100, 0.1)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 10000.0
        algorithm = default_algorithm(0.0012 - infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(100000) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, p = {2}, inf_p = 0.0012-0.0002,\n cure_p = {3}, crit = 0.001".format( \
        model.n, avg_node_degree, model.beta, algorithm.p_cure))

    handle_output('no_4_ws.svg', to_file)
    return result

#no_1_ws()
#no_2_ws()
#no_3_ws()
#no_4_ws()

def no_1_ba(to_file=True):
    model = ba_model(150000, 100)
    algorithm = default_algorithm(0.05, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_1_ba.svg', to_file)
    return result

def no_2_ba(to_file=True):
    model = ba_model(150000, 100)
    algorithm = default_algorithm(0.0005, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_2_ba.svg', to_file)
    return result

def no_3_ba(to_file=True):
    model = ba_model(150000, 100)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 10000.0
        algorithm = default_algorithm(0.0012 + infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(10) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.0012-0.0022,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_3_ba.svg', to_file)
    return result

def no_4_ba(to_file=True):
    model = ba_model(150000, 100)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 40000.0
        algorithm = default_algorithm(0.0004 - infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.00035-0.0001,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_4_ba.svg', to_file)
    return result

#no_1_ba()
#no_2_ba()
#no_3_ba()
#no_4_ba()

def no_1_baout_wsin(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model2, model1)
    algorithm = default_algorithm(0.05, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_1_baout_wsin.svg', to_file)
    return result

def no_2_baout_wsin(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model2, model1)
    algorithm = default_algorithm(0.0005, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_2_baout_wsin.svg', to_file)
    return result

def no_3_baout_wsin(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model2, model1)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 10000.0
        algorithm = default_algorithm(0.0012 + infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(10) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.0012-0.0022,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_3_baout_wsin.svg', to_file)
    return result

def no_4_baout_wsin(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model2, model1)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 40000.0
        algorithm = default_algorithm(0.0004 - infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.00035-0.0001,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_4_baout_wsin.svg', to_file)
    return result

#no_1_baout_wsin()
#no_2_baout_wsin()
#no_3_baout_wsin()
#no_4_baout_wsin()

def no_1_wsout_bain(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model1, model2)
    algorithm = default_algorithm(0.05, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_1_wsout_bain.svg', to_file)
    return result

def no_2_wsout_bain(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model1, model2)
    algorithm = default_algorithm(0.0005, 0.1) #lambda_crit = 0.001
    simulation = sis.sis_simulator(model, algorithm)
    simulation.set_converge_epsilon(0.5)
    simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
    result = simulation.get_proportionalized_output()
    newplot()
    avg_node_degree = (2.0 * len(model.g.edges())) / model.n
    plot.title("N = {0}, m = {1}, inf_p = {2},\n cure_p = {3}".format( \
        model.n, avg_node_degree,algorithm.p_infect, algorithm.p_cure))
    plot.plot(result)
    handle_output('no_2_wsout_bain.svg', to_file)
    return result

def no_3_wsout_bain(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model1, model2)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 10000.0
        algorithm = default_algorithm(0.0012 + infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(10) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.0012-0.0022,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_3_wsout_bain.svg', to_file)
    return result

def no_4_wsout_bain(to_file=True):
    model1 = ba_model(15000, 100)
    model2 = ws_model(15000, 100, 0.1)
    model = ba_ws_inout(model1, model2)
    newplot()
    avg_node_degree = 100

    for infpi in range(1, 10):
        infpf = infpi / 40000.0
        algorithm = default_algorithm(0.0004 - infpf, 0.1) #lambda_crit = 0.001
        simulation = sis.sis_simulator(model, algorithm)
        simulation.set_converge_epsilon(0.5)
        simulation.run_simulation(100) #100 initial infected nodes, since spreading chances are reallzy low
        #result = average_n_runs(simulation, 100)
        result = simulation.get_proportionalized_output()
        plot.plot(result)

    plot.title("N = {0}, m = {1}, inf_p = 0.00035-0.0001,\n cure_p = {2}".format( \
        model.n, avg_node_degree, algorithm.p_cure))

    handle_output('no_4_wsout_bain.svg', to_file)
    return result

no_1_wsout_bain()
no_2_wsout_bain()
no_3_wsout_bain()
no_4_wsout_bain()

#model_in = ws_model(15000, 5, 0.05)
#model_out = ba_model(15000, 10)
#model = ba_ws_inout(model_out, model_in)

#algorithm = default_algorithm(0.1, 0.3)
#simulation = sis.sis_simulator(model, algorithm)
#simulation.set_converge_epsilon(0.5)
#simulation.set_end_criteria_avg_length(50)
#result = average_n_runs(simulation, 10)


#plot.yscale('log')
#plot.xscale('log')
#plot.plot(average_n_runs(simulation, 10))
#plt.show()
