#!/bin/python

import networkx as nx
import random
import math

class infection_algorithm:
    def infect_new_nodes(self, graph, infected_nodes):
        result = set()
        return result

    def cure_nodes(self, graph, infected_nodes):
        result = set()
        return result


class network_model:
    def get_graph(self):
        return graph()    


class sis_simulator:
    def __init__(self, network_model, spreading_algorithm):
        self.model = network_model
        self.algorithm = spreading_algorithm 
        self.avg_criteria_to = 100
        self.crit_reached = False
        self.epsilon = 0.01
        self.last_counts = list([0])
        self.has_run = False
        self.averager = 0.0

    print_progress = False
    
    def equilibrium(self):
        if not self.crit_reached:
            len_last_counts = len(self.last_counts)
            if len_last_counts <= self.avg_criteria_to:
                if sis_simulator.print_progress:
                    print "Fill average: ", ((len_last_counts * 100.0) / self.avg_criteria_to)
                return False
            elif len_last_counts == self.avg_criteria_to + 1:
                self.crit_reached = True
                for i in range(-self.avg_criteria_to - 1, -2):
                    self.averager += self.last_counts[i+1] - self.last_counts[i]

        self.averager -= self.last_counts[-self.avg_criteria_to] - self.last_counts[-self.avg_criteria_to - 1]
        self.averager += self.last_counts[-1] - self.last_counts[-2]

        average = self.averager / (self.avg_criteria_to - 1)
        if (abs(average) < self.epsilon):
            return True
        else:
            if sis_simulator.print_progress:
                print "Konverging: ", average
            return False

    def run_simulation(self, init_select=1):
        if self.has_run:
            return self.last_counts

        self.has_run = True
        g = self.model.get_graph()

        infected = set(random.sample(g.nodes(), init_select))
        self.last_counts.append(init_select)

        while not self.equilibrium():
            newly_infected = self.algorithm.infect_new_nodes(g, infected)
            cured_nodes = self.algorithm.cure_nodes(g, infected)

            infected.difference_update(cured_nodes)
            infected.update(newly_infected)

            self.last_counts.append(len(infected))

        return self.last_counts

    def get_proportionalized_output(self):
        divide = list()
        for item in self.last_counts:
            divide.append((item * 1.0) / self.model.n)
        return divide

    def get_equilibrium_proportion(self):
        return math.fsum(self.last_counts[-self.avg_criteria_to:]) / (self.avg_criteria_to * self.model.n)

    def get_total_infected(self):
        return self.last_counts[-1]

    def get_infected_by_timestep(self):
        return self.last_counts

    def set_end_criteria_avg_length(self, length):
        self.avg_criteria_to = length

    def set_converge_epsilon(self, epsilon):
        if (epsilon > 0):
            self.epsilon = epsilon

    def set_network_model(self, network_model):
        self.model = network_model;

    def set_spreading_algorithm(self, spreading_algorithm):
        self.algorithm = spreading_algorithm

    def set_print_koverge(self, do_print):
        sis_simulator.print_progress = do_print

    
